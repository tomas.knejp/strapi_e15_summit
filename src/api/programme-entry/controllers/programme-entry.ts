/**
 * programme-entry controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::programme-entry.programme-entry');
