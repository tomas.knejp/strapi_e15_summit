/**
 * programme-entry service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::programme-entry.programme-entry');
