import type { Schema, Attribute } from '@strapi/strapi';

export interface AboutEnticementLargeText extends Schema.Component {
  collectionName: 'components_about_enticement_large_texts';
  info: {
    displayName: 'LargeText';
    icon: 'bold';
  };
  attributes: {
    Text: Attribute.String;
  };
}

export interface AboutEnticementSmallText extends Schema.Component {
  collectionName: 'components_about_enticement_small_texts';
  info: {
    displayName: 'SmallText';
    icon: 'underline';
  };
  attributes: {
    Text: Attribute.String;
  };
}

export interface NavMenuItem extends Schema.Component {
  collectionName: 'components_nav_menu_items';
  info: {
    displayName: 'MenuItem';
    icon: 'bulletList';
  };
  attributes: {};
}

export interface PartnerSectionPartners extends Schema.Component {
  collectionName: 'components_partner_section_partners';
  info: {
    displayName: 'Partners';
    icon: 'apps';
  };
  attributes: {
    Logo: Attribute.Media & Attribute.Required;
    URL: Attribute.String & Attribute.Required;
  };
}

export interface SocialLinksFacebook extends Schema.Component {
  collectionName: 'components_social_links_facebooks';
  info: {
    displayName: 'Facebook';
    icon: 'earth';
  };
  attributes: {
    URL: Attribute.String & Attribute.Required;
  };
}

export interface SocialLinksInstagram extends Schema.Component {
  collectionName: 'components_social_links_instagrams';
  info: {
    displayName: 'Instagram';
    icon: 'earth';
    description: '';
  };
  attributes: {
    URL: Attribute.String & Attribute.Required;
  };
}

export interface SocialLinksLinkedIn extends Schema.Component {
  collectionName: 'components_social_links_linked_ins';
  info: {
    displayName: 'LinkedIn';
    icon: 'earth';
  };
  attributes: {
    URL: Attribute.String & Attribute.Required;
  };
}

export interface SocialLinksTwitter extends Schema.Component {
  collectionName: 'components_social_links_twitters';
  info: {
    displayName: 'Twitter';
    icon: 'earth';
  };
  attributes: {
    URL: Attribute.String & Attribute.Required;
  };
}

export interface TicketTicketContent extends Schema.Component {
  collectionName: 'components_ticket_ticket_contents';
  info: {
    displayName: 'TicketContent';
    icon: 'bulletList';
  };
  attributes: {
    Text: Attribute.String & Attribute.Required;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'about-enticement.large-text': AboutEnticementLargeText;
      'about-enticement.small-text': AboutEnticementSmallText;
      'nav.menu-item': NavMenuItem;
      'partner-section.partners': PartnerSectionPartners;
      'social-links.facebook': SocialLinksFacebook;
      'social-links.instagram': SocialLinksInstagram;
      'social-links.linked-in': SocialLinksLinkedIn;
      'social-links.twitter': SocialLinksTwitter;
      'ticket.ticket-content': TicketTicketContent;
    }
  }
}
